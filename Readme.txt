Overview

Configuration Helper

Simplify the task of changing some behaviors of CKEditor:

Hide dialog fields
Remove dialog fields
Set a 'placeholder' when the editor is empty

Reference:http://ckeditor.com/addon/confighelper
More details: https://alfonsoml.blogspot.com.es/2012/04/placeholder-text-in-ckeditor.html


INSTALLATION

1) Download and install the Configuration Helper module as usual.
2) Download the Configuration Helper plugin from 
                 http://ckeditor.com/addon/confighelper
3) Unzip the plugin in the 'plugin' folder. The main Javascript file of 
   the module should be in 'confighelper/plugin/confighelper/plugin.js'
   OR you can use drush.
4) Enable the plugin in the Wysiwyg profile configuration page.
5) Add HTML5 placeholder attribute in the textarea that use Wysiwyg.
6) Finally the placeholder text appears on the Wysiwyg CKEditor

Tested with modules:

1) Field placeholder

