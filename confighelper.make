api = 2
core = 7.x

libraries[confighelper][download][type] = get
libraries[confighelper][download][url] = http://download.ckeditor.com/confighelper/releases/confighelper_1.8.3.zip
; This path is specific to each drupal's install
libraries[confighelper][destination] = modules/contrib/base/confighelper/plugins